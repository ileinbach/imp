package main

import (
	"gitlab.com/ileinbach/imp/frontend"
//	"gitlab.com/ileinbach/imp/backend"
	"gitlab.com/ileinbach/imp/config"
)

type Compiler struct{
	frontend frontend.Frontend
//	backend  backend.Backend
	conf     *config.Config
}

// Returns a configured, ready-to-use Compiler.
func NewCompiler(conf *config.Config) *Compiler {
	return &Compiler{
		frontend: frontend.NewFrontend(conf),
//		backend: backend.NewBackend(conf),
		conf: conf,
	}
}

// Returns a binary generated from compiling imp source code.
func (c *Compiler) Compile(src string) ([]byte, error) {
	_, err := c.frontend.Frontend(src)
	if err != nil {
		return nil, err
	}

//	bin, err := c.backend.Backend(prog)
//	if err != nil {
//		return nil, err
//	}
//
//  return bin, nil

	return nil, nil
}
