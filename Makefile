.PHONY: clean test all

all: imp twerp

imp: frontend/* config/* errors/* utils/*
	go build -o imp

twerp: imp interp/*.go
	go build -o twerp interp/*.go

frontend/lexer.go: frontend/parser.go

frontend/parser.go: frontend/parser.y
	go get -u golang.org/x/tools/cmd/goyacc
	go generate -x

test: imp twerp
	@echo ""
	@echo "Compiling Examples"
	@echo "=================="
	@./imp examples/*.imp
	@echo ""
	@echo "Interpreting Examples"
	@echo "====================="
	@./twerp examples/*.imp
	@echo ""

clean:
	$(RM) frontend/{y.output,parser.go}
	$(RM) imp twerp
