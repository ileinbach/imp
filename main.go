package main

//go:generate goyacc -l -o frontend/parser.go -v frontend/y.output frontend/parser.y

import (
	"io/ioutil"
	"os"
)

import (
	"gitlab.com/ileinbach/imp/config"
	"gitlab.com/ileinbach/imp/errors"
)

func main() {
	// Nothing to do.
	if len(os.Args) <= 1 {
		errors.None()
	}

	conf, args := config.Parse(os.Args[1:])
	if conf.Help {
		errors.None()
	}

	imp := NewCompiler(conf)

	for _, filename := range args {
		src, err := ioutil.ReadFile(filename)
		if err != nil {
			errors.Failure(err)
		}

		_, err = imp.Compile(string(src))
		if err != nil {
			errors.Failure(err)
		}

		errors.Success(filename)
	}

	errors.None()
}
