package config

import (
	"reflect"
	"flag"
	"strings"
	"unicode"
)

var Default = Config{
	RegCount: 8,
	ArgCount: 6,
}

type Config struct{
	LexVerbosity       int    `usage:"level of debugging information to print while lexing"`
	ParseVerbosity     int    `usage:"level of debugging information to print while parsing"`
	FlattenVerbosity   int    `usage:"level of debugging information to print while flattening"`
	TargetArchitecture string `usage:"target architecture for code generation"`
	RegCount           int    `usage:"number of registers available"`
	ArgCount           int    `usage:"number of arguments a function can accept"`
	Help               bool   `usage:"print help info"`
}

// Returns a populated Config alongside non-flag arguments.  Reflection is used
// to define flags according to the Config's type definition itself.
func Parse(args []string) (*Config, []string) {
	conf := new(Config)
	confValue := reflect.ValueOf(conf).Elem()
	confType := reflect.TypeOf(conf).Elem()

	flagset := flag.NewFlagSet("imp", flag.PanicOnError)
	flagsetValue := reflect.ValueOf(flagset)

	// Build flagset from Config struct.
	for i := 0; i < confType.NumField(); i++ {
		fieldValue := confValue.Field(i)
		fieldType := confType.Field(i)

		typ := strings.Title(fieldType.Type.Name())
		defineFlag := flagsetValue.MethodByName(typ + "Var")
		if !defineFlag.IsValid() {
			panic("config field type unsupported by Go flag library: " + typ)
		}

		short, long := flagNames(fieldType.Name)
		usage := fieldType.Tag.Get("usage")

		defineFlag.Call([]reflect.Value{
			fieldValue.Addr(),
			reflect.ValueOf(short),
			reflect.ValueOf(Default).Field(i),
			reflect.ValueOf(usage),
		})
		defineFlag.Call([]reflect.Value{
			fieldValue.Addr(),
			reflect.ValueOf(long),
			reflect.ValueOf(Default).Field(i),
			reflect.ValueOf(usage),
		})
	}

	flagset.Parse(args)
	if conf.Help {
		flagset.PrintDefaults()
	}

	return conf, flagset.Args()
}

// Returns the short and long flag names given a field name.
func flagNames(fieldName string) (string, string) {
	var shortName, longName strings.Builder
	var hasUpper, hasLower bool

	// Ignores non-letters.
	for _, rn := range []rune(fieldName) {
		if unicode.IsUpper(rn) {
			hasUpper = true
			shortName.WriteRune(unicode.ToLower(rn))
			longName.WriteRune('-')
			longName.WriteRune(unicode.ToLower(rn))
		} else if unicode.IsLower(rn) {
			hasLower = true
			longName.WriteRune(rn)
		} else {
			panic("invalid config item name: " + fieldName)
		}
	}

	// Avoids empty names and ambiguity between short and long.
	if !(hasUpper && hasLower) {
		panic("invalid config item name: " + fieldName)
	}

	return shortName.String(), longName.String()[1:]
}
