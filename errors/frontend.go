package errors

import (
	"fmt"
)

import (
	"gitlab.com/ileinbach/imp/utils"
)

func UnrecognizedInput(rn rune) error {
	return New(fmt.Sprintf("unrecognized input: %s\n", utils.Repr(rn)))
}

func Undefined(t Textual) error {
	return New(fmt.Sprintf("undefined %s: %s", t.Type(), t))
}

func Unsupported(format string, a ...interface{}) error {
	return New("unsupported feature: " + format, a...)
}

func TypeMismatch(expected, found Typed) error {
	return New(fmt.Sprintf(
		"type mismatch: can't get %s from %s",
		expected.Type(), found.Type(),
	))
}

func CountMismatch(expected, found int) error {
	return New(fmt.Sprintf(
		"count mismatch: expected %d but found %d",
		expected, found,
	))
}
