package errors

func UnparseableFlag(name string, value interface{}) error {
	return New("unparseable flag: %s=%s\n", name, value)
}
