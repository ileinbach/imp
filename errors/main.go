package errors

import (
	"fmt"
	"os"
)

// Prints and imp error message.
func Print(err error) {
	fmt.Fprintf(os.Stderr, "imp: %s\n", err)
}

// Prints a message to report successful compilation.
func Success(filename string) {
	fmt.Printf("Source file \"%s\" compiled with no errors.\n", filename)
}

// Exits program upon failed execution.
func Failure(err error) {
	Print(err)
	os.Exit(1)
}

// Exits program upon successful execution.
func None() {
	os.Exit(0)
}

func BadSourceFile(filename string, err error) error {
	return New(fmt.Sprintf("error opening %s: %v", filename, err))
}

