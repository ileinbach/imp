package errors

import (
	"errors"
	"fmt"
)

type Textual interface {
	String() string
	Pos() int
	Typed
}

type Typed interface {
	Type() string
}

// Wrapper for New() from Go standard library that adds string formatting.
func New(format string, a ...interface{}) error {
	return errors.New(fmt.Sprintf(format, a...))
}

// Wraps an error with the context of a line.
func Line(line int, err error) error {
	return New("line %d: %s", line, err)
}

// Wraps an error with the context of a Textual object.
func Wrap(err error, t Textual) error {
	return New(fmt.Sprintf("%s at %d (%s): %s", t.Type(), t.Pos(), t, err))
}
