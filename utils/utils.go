package utils

import (
	"strings"
)

// Returns a string representation of a rune that's easier to read.
func Repr(rn rune) string {
	switch rn {
	case '\n':
		return "CR"
	case '\t':
		return "TAB"
	default:
		return string(rn)
	}
}

// Returns a string with a prefix inserted at the beginning of each line.
func PrefixLines(s, prefix string) string {
	var b strings.Builder

	for next := 0; len(s) > 0; s = s[next:] {
		b.WriteString(prefix)
		next = strings.IndexRune(s, '\n') + 1
		if next == 0 {
			b.WriteString(s)
			break
		}
		b.WriteString(s[:next])
	}

	return b.String()
}

func Indent(s string) string {
	return PrefixLines(s, "\t")
}

func Quote(s string) string {
	return `"` + s + `"`
}
