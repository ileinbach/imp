package main

import (
	"flag"
	"fmt"
	"io/ioutil"
)

import (
	"gitlab.com/ileinbach/imp/frontend"
	"gitlab.com/ileinbach/imp/config"
	"gitlab.com/ileinbach/imp/errors"
)

var interactiveMode bool

const interactiveModeUsage string = "interpreter blocks on each pseudo-instruction with options for querying internal state"

func init() {
	flag.BoolVar(&interactiveMode, "i", false, interactiveModeUsage)
}

func main() {
	flag.Parse()

	conf := &config.Default
	f := frontend.NewFrontend(conf)

	for _, filename := range flag.Args() {
		src, err := ioutil.ReadFile(filename)
		if err != nil {
			errors.Failure(errors.BadSourceFile(filename, err))
		}

		psuedo, err := f.Frontend(string(src))
		if err != nil {
			errors.Failure(err)
		}

/*
		fmt.Println("START OF PROGRAM")
		fmt.Println(frontend.DumpProgram(psuedo))
		fmt.Println("END OF PROGRAM")
*/

		imptwerpreter := NewTwerp(psuedo, conf.RegCount)
		ret, err := imptwerpreter.Exec(interactiveMode)
		if err != nil {
			errors.Failure(err)
		}

		fmt.Printf("Imptwerpreter returned successfully with %v.\n", ret)
	}
}
