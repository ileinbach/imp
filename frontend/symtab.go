package frontend

import (
	"gitlab.com/ileinbach/imp/errors"
)

type SymbolTable interface{
	Enter(Decl) error
	Exit()

	// Expose Scope API for local scope.
	Scope
}

type symbolTable struct{
	scopes []Scope
}

// Returns a new SymbolTable initialized with a global Scope.
func NewSymbolTable(global Scope) SymbolTable {
	if global == nil {
		panic("cannot create SymbolTable with nil global Scope")
	}
	return &symbolTable{
		scopes: []Scope{global},
	}
}

//
// Symtab API
//

func (s *symbolTable) Enter(decl Decl) error {
	local, err := NewScope(decl)
	if err != nil {
		return err
	}
	s.scopes = append(s.scopes, local)
	return nil
}

func (s *symbolTable) Exit() {
	d := s.depth()
	if d == 0 {
		panic("tried to call (*symbolTable).exit() in outermost scope")
	}
	s.scopes = s.scopes[:d]
}

//
// Scope API
//

func (s *symbolTable) Callee() Cmd {
	return s.local().Callee()
}

func (s *symbolTable) Define(a Alias, v Value) error {
	return s.local().Define(a, v)
}

func (s *symbolTable) Lookup(a Alias) (v Value, err error) {
	for d := s.depth(); d >= 0; d-- {
		v, err = s.scopes[d].Lookup(a)
		if err == nil { return }
	}
	return nil, errors.Undefined(a)
}

//
// Private Methods
//

// Returns depth of local scope.
func (s *symbolTable) depth() int {
	if len(s.scopes) == 0 {
		panic("tried to call (*symbolTable).depth() without any scopes")
	}
	return len(s.scopes) - 1
}

// Returns current local scope.
func (s *symbolTable) local() Scope {
	return s.scopes[s.depth()]
}
