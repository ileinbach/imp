package frontend

import (
	"fmt"
	"strings"
)

//
// Psuedo-values
//

type (
	Value interface {
		Value()
		String() string
		Type() string
	}
	Reg int
	Num int64
	Cmd struct {
		Addr   Num
		Params []Value
	}
)

func (r Reg) Value() {}
func (n Num) Value() {}
func (c Cmd) Value() {}

func (r Reg) String() string {
	return fmt.Sprint(int(r))
}

func (n Num) String() string {
	return fmt.Sprint(int64(n))
}

func (c Cmd) String() string {
	var b strings.Builder

	b.WriteString(fmt.Sprintf("[Addr: %d, Params:", c.Addr))
	for _, param := range c.Params {
		b.WriteString(fmt.Sprintf(" %s", param.Type()))
	}
	b.WriteString("]")

	return b.String()
}

func (r Reg) Type() string { return "Reg" }
func (n Num) Type() string { return "Num" }
func (c Cmd) Type() string { return "Cmd" }

//
// Psuedo-instructions
//

type (
	Instr interface{
		Instr() string
		String() string
		Type() string
	}
	Ins struct{
		Name    string
		Args    []Value
		Comment string
	}
)

func (i Ins) Instr() string {
	return i.Name
}

func (i Ins) String() string {
	var b strings.Builder
	b.WriteString(i.Name)
	for _, arg := range i.Args {
		b.WriteString(fmt.Sprintf(" %v", arg))
	}
	if len(i.Comment) > 0 {
		b.WriteString(fmt.Sprintf("    # %s", i.Comment))
	}
	return b.String()
}

func (i Ins) Type() string {
	return "Ins"
}

func (i Ins) WithComment(format string, a ...interface{}) Ins {
	i.Comment = fmt.Sprintf(format, a...)
	return i
}
