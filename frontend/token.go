package frontend

type Token interface{
	Lexeme() string
	Line() int
}

type token struct{
	lexeme string
	line   int
}

func NewToken(lexeme string, line int) Token {
	return &token{
		lexeme: lexeme,
		line:   line,
	}
}

func (t *token) Lexeme() string {
	return t.lexeme
}

func (t *token) Line() int {
	return t.line
}
