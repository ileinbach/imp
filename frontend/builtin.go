package frontend

import (
	"gitlab.com/ileinbach/imp/errors"
)

// Builtin procedures are represented in the compiler as InstrFuncs. Each
// performs its own typechecking. Returns the number of psuedo-instructions
// generated.
type InstrFunc func(*frontend, ...Value) (int, error)

// Map of all builtin procedures.
var Builtins map[string]InstrFunc

func init() {
	Builtins = map[string]InstrFunc{
		"add": (*frontend).add,
		"mov": (*frontend).mov,
		"rec": (*frontend).rec,
		"ret": (*frontend).ret,
		"sub": (*frontend).sub,
	}
}

func (f *frontend) add(args ...Value) (int, error) {
	if len(args) != 2 {
		return 0, errors.New("add expects 2 arguments")
	}

	dst, ok := args[1].(Reg)
	if !ok {
		return 0, errors.New("dst argument of add must be a register")
	}

	var n int
	switch src := args[0].(type) {
	case Reg:
		n = f.emit(Ins{
			Name: "ADD_R",
			Args: []Value{ src, dst },
		})
	case Num:
		n = f.emit(Ins{
			Name: "ADD_I",
			Args: []Value{ src, dst },
		})
	default:
		return 0, errors.New("src argument of add must be a register or number")
	}
	return n, nil
}

func (f *frontend) mov(args ...Value) (int, error) {
	if len(args) != 2 {
		return 0, errors.New("mov expects 2 arguments")
	}

	dst, ok := args[1].(Reg)
	if !ok {
		return 0, errors.New("dst argument of mov must be a register")
	}

	var n int
	switch src := args[0].(type) {
	case Reg:
		n = f.emit(Ins{
			Name: "MOVE_R",
			Args: []Value{ src, dst },
		})
	case Num:
		n = f.emit(Ins{
			Name: "MOVE_I",
			Args: []Value{ src, dst },
		})
	default:
		return 0, errors.New("invalid src type: %s", src.Type())
	}
	return n, nil
}

func (f *frontend) rec(args ...Value) (int, error) {
	if len(args) != 0 && len(args) != 2 {
		return 0, errors.New("ret expects either 0 or 2 arguments")
	}

	var n int
	if len(args) == 2 {
		right, ok := args[1].(Reg)
		if !ok {
			return 0, errors.New("right argument of ret must be a register")
		}
		switch left := args[0].(type) {
		case Reg:
			n = f.emit(Ins{
				Name: "BNE_R",
				Args: []Value{ left, right, f.here()+2 },
			})
		case Num:
			n = f.emit(Ins{
				Name: "BNE_I",
				Args: []Value{ left, right, f.here()+2 },
			})
		default:
			return 0, errors.New("left argument of ret must be a register or number")
		}
	}
	n += f.emit(Ins{
		Name: "CALL_I",
		Args: []Value{ f.symtab.Callee().Addr },
	})

	return n, nil
}

func (f *frontend) ret(args ...Value) (int, error) {
	if len(args) == 0 {
		return f.emit(Ins{ Name: "RET" }), nil
	}
	if len(args) != 2 {
		return 0, errors.New("ret expects either 0 or 2 arguments")
	}

	right, ok := args[1].(Reg)
	if !ok {
		return 0, errors.New("right argument of ret must be a register")
	}

	var n int
	switch left := args[0].(type) {
	case Reg:
		n = f.emit(Ins{
			Name: "BNE_R",
			Args: []Value{ left, right, f.here()+2 },
		})
	case Num:
		n = f.emit(Ins{
			Name: "BNE_I",
			Args: []Value{ left, right, f.here()+2 },
		})
	default:
		return 0, errors.New("left argument of ret must be a register or number")
	}
	n += f.emit(Ins{ Name: "RET" })
	return n, nil
}

func (f *frontend) sub(args ...Value) (int, error) {
	if len(args) != 2 {
		return 0, errors.New("sub expects 2 arguments")
	}

	dst, ok := args[1].(Reg)
	if !ok {
		return 0, errors.New("dst argument of sub must be a register")
	}

	var n int
	switch src := args[0].(type) {
	case Reg:
		n = f.emit(Ins{
			Name: "SUB_R",
			Args: []Value{ src, dst },
		})
	case Num:
		n = f.emit(Ins{
			Name: "SUB_I",
			Args: []Value{ src, dst },
		})
	default:
		return 0, errors.New("src argument of sub must be a register or number")
	}
	return n, nil
}
