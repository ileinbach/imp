%{

package frontend

import (
	"gitlab.com/ileinbach/imp/config"
)

type Parser interface{
	Parse() error
	Debug
}

type parser struct{
	lexer Lexer
	ast   *[]Stmt

	conf  *config.Config
}

func NewParser(f *frontend) Parser {
	return &parser{
		conf: f.conf,
		lexer: f.lexer,
		ast: &f.ast,
	}
}

func (p *parser) Parse() error {
	iiparse = p
	yyParse(p.lexer)
	return p.lexer.(*lexer).err
}

var iiparse Parser

%}

%union{
	tok      Token

	arg     Alias
	arglist []Alias

	stmt     Stmt
	stmtlist []Stmt
}

%token <tok> CMD REG NUM CMT CR

%type <arg> arg
%type <arglist> args maybe_args

%type <stmt> stmt decl call
%type <stmtlist> program main

%start main

%%

main:
	/* nullable */ {
		// See Parse stage for details of this workaround.
		*(iiparse.(*parser)).ast = []Stmt{}

		iiparse.Debug(1, true, "main -> EPSILON\n")
	}
|
	program {
		// See Parse stage for details of this workaround.
		*(iiparse.(*parser)).ast = $1

		iiparse.Debug(1, true, "main -> program\n")
		iiparse.Debug(1, false, "\n")
		iiparse.Debug(2, true, DumpAst($1))
		iiparse.Debug(2, false, "\n\n")
	}

program:
	program stmt {
		if $2 == nil {
			$$ = $1
		} else {
			$$ = append($1, $2)
		}
		iiparse.Debug(1, true, "program -> program stmt\n")
	}
|
	stmt {
		if $1 == nil {
			$$ = []Stmt{}
		} else {
			$$ = []Stmt{$1}
		}
		iiparse.Debug(1, true, "program -> stmt\n")
	}

stmt:
	decl delim {
		$$ = $1
		iiparse.Debug(1, true, "stmt -> decl delim\n")
	}
|
	call delim {
		$$ = $1
		iiparse.Debug(1, true, "stmt -> call delim\n")
	}
|
	comment delim {
		$$ = nil
		iiparse.Debug(1, true, "stmt -> comment delim\n")
	}

decl:
	':' CMD maybe_args '{' delim program '}' {
		cmd := CmdAlias{$2.Lexeme(), $2.Line()}
		$$ = Decl{cmd, $3, $6}
		iiparse.Debug(1, true, "decl -> :CMD maybe_args { delim program }\n")
	}

call:
	CMD maybe_args {
		cmd := CmdAlias{$1.Lexeme(), $1.Line()}
		$$ = Call{cmd, $2}
		iiparse.Debug(1, true, "call -> CMD maybe_args\n")
	}

comment:
	CMT {
		iiparse.Debug(1, true, "comment -> CMT\n")
	}

maybe_args:
	/* nullable */ {
		$$ = make([]Alias, 0, 0)
		iiparse.Debug(1, true, "maybe_args -> EPSILON\n")
	}
|
	args {
		$$ = $1
		iiparse.Debug(1, true, "maybe_args -> args\n")
	}

args:
	args ',' arg {
		$$ = append($1, $3)
		iiparse.Debug(1, true, "args -> args, arg\n")
	}
|
	arg {
		$$ = []Alias{$1}
		iiparse.Debug(1, true, "args -> arg\n")
	}

arg:
	REG {
		$$ = RegAlias{$1.Lexeme(), $1.Line()}
		iiparse.Debug(1, true, "arg -> REG\n")
	}
|
	NUM {
		$$ = NumAlias{$1.Lexeme(), $1.Line()}
		iiparse.Debug(1, true, "arg -> NUM\n")
	}

delim:
	CR delim {
		iiparse.Debug(1, true, "delim -> CR delim\n")
	}
|
	CR {
		iiparse.Debug(1, true, "delim -> CR\n")
	}
