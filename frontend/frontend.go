package frontend

import (
	"strconv"
)

import (
	"gitlab.com/ileinbach/imp/config"
)

type Frontend interface{
	Frontend(string) ([]Instr, error)
}

type frontend struct{
	src  string
	ast  []Stmt
	prog []Instr

	lexer  Lexer
	parser Parser
	symtab SymbolTable

	conf *config.Config
	err  error
}

func NewFrontend(conf *config.Config) Frontend {
	return &frontend{
		conf: conf,
	}
}

func (f *frontend) Frontend(src string) ([]Instr, error) {
	f.src = src

	if f.Lex(); f.err != nil {
		return nil, f.err
	}
	if f.Parse(); f.err != nil {
		return nil, f.err
	}
	if f.Flatten(); f.err != nil {
		return nil, f.err
	}

	f.Debug(1, true, DumpProgram(f.prog))
	return f.prog, nil
}

//
// Frontend Stages
//

// The Lex stage is a bit contrived due to the parser-driven nature of goyacc.
// That is, tokens are lexed as-needed by the parser. That being said, each
// stage is additionally responsible for readying the frontend for the next, so
// there is still work to be done.
func (f *frontend) Lex() error {
	f.lexer = NewLexer(f)
	return nil
}

// The Parse stage essentially just calls into goyacc with a handle to the
// lexer. The limitations of goyacc heavily determine the way in which Parser
// wrapper operates under the hood.
func (f *frontend) Parse() error {
	f.parser = NewParser(f)
	f.err = f.parser.Parse()
	return f.err
}

func (f *frontend) Flatten() error {
	f.symtab = NewSymbolTable(GlobalScope())
	f.prog = []Instr{}
	_, f.err = f.flatten(f.ast)
	return f.err
}

//
// Internal Methods
//

// Returns Value(s) associated with Alias(es). Aliases can either be literals
// (whose value is parsed from the alias itself) or a variable name (whose
// associated Value is defined in some Scope).
func (f *frontend) eval(aliases ...Alias) ([]Value, error) {
	values := make([]Value, len(aliases))

	for i, alias := range aliases {
		// Detect Num literals.
		if _, ok := alias.(NumAlias); ok {
			num, err := strconv.ParseInt(alias.String(), 0, 0)
			if err == nil {
				values[i] = Num(num)
				continue
			}
		}

		// Look for bound name.
		if value, err := f.symtab.Lookup(alias); err == nil {
			values[i] = value
		} else {
			return nil, err
		}
	}

	return values, nil
}

// Appends psuedo-instructions to an internal slice. Returns the number of
// psuedo-instructions appended.
func (f *frontend) emit(i ...Instr) int {
	//f.Debug(1, true, DumpProgram(i))
	f.prog = append(f.prog, i...)
	return len(i)
}

// Returns the psuedo-address (index into f.prog) of the next-to-be-emitted
// psuedo-instruction.
func (f *frontend) here() Num {
	return Num(len(f.prog))
}
