package frontend

import (
	"gitlab.com/ileinbach/imp/errors"
)

type Scope interface{
	Callee() Cmd
	Lookup(Alias) (Value, error)
	Define(Alias, Value) error
}

type scope struct{
	cmds map[string]Cmd
	regs map[string]Reg
	nums map[string]Reg
}

// Returns a new Scope initialized with parameters of a declaration.
func NewScope(decl Decl) (Scope, error) {
	var err error
	s := &scope{
		cmds: make(map[string]Cmd),
		regs: make(map[string]Reg),
		nums: make(map[string]Reg),
	}

	for i, param := range decl.Params {
		err = s.Define(param, Reg(i))
		if err != nil {
			return nil, err
		}
	}

	return s, nil
}

func (s *scope) Callee() Cmd {
	if cmd, ok := s.cmds["__callee__"]; ok {
		return cmd
	}
	panic("__callee__ missing from scope")
}

func (s *scope) Lookup(a Alias) (Value, error) {
	switch a := a.(type) {
	case CmdAlias:
		if cmd, ok := s.cmds[a.String()]; ok {
			return cmd, nil
		}
	case RegAlias:
		if reg, ok := s.regs[a.String()]; ok {
			return reg, nil
		}
	case NumAlias:
		if reg, ok := s.nums[a.String()]; ok {
			return reg, nil
		}
	}
	return nil, errors.Undefined(a)
}

func (s *scope) Define(a Alias, v Value) error {
	// fmt.Printf("%s(%s) -> %s(%s)\n", a.Type(), a, v.Type(), v)

	switch a := a.(type) {
	case CmdAlias:
		if cmd, ok := v.(Cmd); ok {
			s.cmds[a.String()] = cmd
			return nil
		}
	case RegAlias:
		if reg, ok := v.(Reg); ok {
			s.regs[a.String()] = reg
			return nil
		}
	case NumAlias:
		if reg, ok := v.(Reg); ok {
			s.nums[a.String()] = reg
			return nil
		}
	default:
		panic("dynamic type of Alias unrecognized: " + a.String())
	}
	return errors.TypeMismatch(v, a)
}

func GlobalScope() Scope {
	return &scope{
		cmds: map[string]Cmd{
			"__callee__": Cmd{
				Addr: Num(0),
				Params: nil,
			},
		},
		regs: map[string]Reg{
			"0": Reg(0),
			"1": Reg(1),
			"2": Reg(2),
			"3": Reg(3),
			"4": Reg(4),
			"5": Reg(5),
			"6": Reg(6),
			"7": Reg(7),
		},
		nums: make(map[string]Reg),
	}
}
