package frontend

import (
	"fmt"
	"strings"
)

import (
	"gitlab.com/ileinbach/imp/utils"
)

func DumpAlias(alias Alias) string {
	return fmt.Sprintf(
		"name: \"%s\"\n" +
		"type: %s\n" +
		"line: %d\n",
		alias,
		alias.Type(),
		alias.Pos(),
	)
}

func DumpAliases(aliases []Alias) string {
	if len(aliases) == 0 {
		return ""
	}

	var b strings.Builder

	b.WriteString("\n")
	for i, alias := range aliases {
		b.WriteString(DumpAlias(alias))
		if i < len(aliases) - 1 {
			b.WriteString("\n")
		}
	}

	return b.String()
}

func DumpStmt(stmt Stmt) string {
	switch stmt := stmt.(type) {
	case Call:
		return fmt.Sprintf(
			"name: \"%s\"\n" +
			"type: %s\n" +
			"line: %d\n" +
			"args: [%s]\n",
			stmt,
			stmt.Type(),
			stmt.Pos(),
			utils.Indent(DumpAliases(stmt.Args)),
		)
	case Decl:
		return fmt.Sprintf(
			"name: \"%s\"\n" +
			"type: %s\n" +
			"line: %d\n" +
			"params: [%s]\n" +
			"body: [\n%s]\n",
			stmt,
			stmt.Type(),
			stmt.Pos(),
			utils.Indent(DumpAliases(stmt.Params)),
			utils.Indent(DumpAst(stmt.Body)),
		)
	default:
		panic("unrecognized Stmt implementation")
	}
}

func DumpAst(ast []Stmt) string {
	var b strings.Builder

	for _, stmt := range ast {
		b.WriteString(DumpStmt(stmt))
		b.WriteString("\n")
	}

	return b.String()
}

func DumpScope(s *scope) string {
	var b strings.Builder

	b.WriteString("====================\n")

	b.WriteString("  Registers\n")
	for alias, value := range s.regs {
		b.WriteString(fmt.Sprintf("    @%s = %v\n", alias, value))
	}

	b.WriteString("--------------------\n")

	b.WriteString("  Commands\n")
	for alias, value := range s.cmds {
		b.WriteString(fmt.Sprintf("    %s = %v\n", alias, value))
	}

	b.WriteString("====================\n")

	return b.String()
}

func DumpSymbolTable(symtab *symbolTable) string {
	var b strings.Builder

	b.WriteString("====================\n")
	b.WriteString("       SYMTAB       \n")
	b.WriteString("====================\n\n")
	for _, s := range symtab.scopes {
		b.WriteString(DumpScope(s.(*scope)))
		b.WriteString("\n")
	}
	b.WriteString("====================\n")

	return b.String()
}

func DumpProgram(prog []Instr) string {
	var b strings.Builder

	for i, ins := range prog {
//		fmt.Printf("[META DEBUG] %2d: %s\n", i, ins)
		b.WriteString(fmt.Sprintf("%2d: %s\n", i, ins))
	}
	b.WriteString("\n")

	if out := b.String(); len(out) > 0 {
		return out[:len(out)-1]
	} else {
		return out
	}
}
