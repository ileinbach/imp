package frontend

import (
	"gitlab.com/ileinbach/imp/errors"
)

// Generates psuedo-instructions from an AST and append them to the previously
// generated psuedo-code. Returns the number of psuedo-instructions generated.
func (f *frontend) flatten(ast []Stmt) (int, error) {
	var (
		n, total int
		err      error
	)
	for _, stmt := range ast {
		switch stmt := stmt.(type) {
		case Call: n, err = f.call(stmt)
		case Decl: n, err = f.decl(stmt)
		}
		if err != nil {
			return 0, errors.Wrap(err, stmt)
		}
		total += n
	}
	//f.Debug(1, true, DumpProgram(f.prog))
	return total, nil
}

// Generates psuedo-instructions for a call (whether a defined procedure or
// builtin). Returns the number of psuedo-instructions generated.
func (f *frontend) call(call Call) (int, error) {
	// Look for Cmd in surrounding scopes.
	if value, err := f.symtab.Lookup(call.Cmd); err == nil {
		args, err := f.eval(call.Args...)
		if err != nil {
			return 0, err
		}

		return f.procCall(value.(Cmd), args), nil
	}

	// Look for Cmd as builtin.
	if instrFunc, ok := Builtins[call.String()]; ok {
		args, err := f.eval(call.Args...)
		if err != nil {
			return 0, err
		}

		return instrFunc(f, args...)
	}

	return 0, errors.Undefined(call)
}

// Generates psuedo-instructions for a declaration. Returns the number of
// psuedo-instructions generated.
func (f *frontend) decl(decl Decl) (int, error) {
	// Create parameter template for typechecking call arguments.
	params := make([]Value, len(decl.Params))
	for i, param := range decl.Params {
		switch param := param.(type) {
		case RegAlias: params[i] = Reg(0)
		case NumAlias: params[i] = Num(0)
		default:
			return 0, errors.Unsupported("%s parameters", param.Type())
		}
	}

	var n int

	// Skip jump ensures procedures are run only when called. Save address of
	// skip jump to backfill destination once declaration body length is known.
	skip := f.here()
	n += f.emit(Ins{})

	// Create entry in current scope.
	cmd := Cmd{
		Addr:   f.here(),
		Params: params,
	}
	f.symtab.Define(decl.Cmd, cmd)

	// Create new scope local to declaration body. Provide local reference to
	// procedure being called as __callee__ (for recursion).
	if err := f.symtab.Enter(decl); err != nil {
		return 0, err
	}
	f.symtab.Define(CmdAlias{name: "__callee__"}, cmd)
	defer f.symtab.Exit()

	DumpSymbolTable(f.symtab.(*symbolTable))

	// Generate psuedo-instructions for declaration body.
	if i, err := f.flatten(decl.Body); err != nil {
		return 0, err
	} else {
		n += i
	}

	// Backfill skip jump destination.
	f.prog[skip] = Ins{
		Name: "JUMP_I",
		Args: []Value{ f.here() },
	}

	return n, nil
}

// Generate psuedo-instructions for a call to a procedure. Returns the number of
// psuedo-instructions generated.
func (f *frontend) procCall(cmd Cmd, args []Value) int {
	var n int

	n += f.procCallProlog(args)
	n += f.emit(Ins{
		Name: "CALL_I",
		Args: []Value{ cmd.Addr },
	})
	n += f.procCallEpilog(args)

	return n
}

// Generate psuedo-instructions for the prolog to a call to a procedure.
// Returns the number of psuedo-instructions generated.
func (f *frontend) procCallProlog(args []Value) int {
	// See depSeqs definition for info about dependency sequences.
	regSeqs, numSeqs := f.depSeqs(args)

	var n int

	// Generate psuedo-instructions for handling dep seqs that start with
	// numbers.
	for num, seq := range numSeqs {
		i := len(seq) - 1
		n += f.emit(Ins{
			Name: "PUSH_R",
			Args: []Value{ Reg(seq[i]) },
		})
		for i--; i >= 0; i-- {
			n += f.emit(Ins{
				Name: "MOVE_R",
				Args: []Value{ Reg(seq[i]), Reg(seq[i+1]) },
			})
		}
		n += f.emit(Ins{
			Name: "MOVE_I",
			Args: []Value{ Num(num), Reg(seq[0]) },
		})
	}

	// Generate psuedo-instructions for handling dep seqs that start with
	// registers.
	for reg, seq := range regSeqs {
		i := len(seq) - 1
		n += f.emit(Ins{
			Name: "PUSH_R",
			Args: []Value{ Reg(seq[i]) },
		})
		for i--; i >= 0; i-- {
			n += f.emit(Ins{
				Name: "MOVE_R",
				Args: []Value{ Reg(seq[i]), Reg(seq[i+1]) },
			})
		}

		// Handle cyclic dep seqs.
		if seq[len(seq)-1] == reg {
			n += f.emit(Ins{
				Name: "POP_R",
				Args: []Value{ Reg(seq[0]) },
			})
		} else {
			n += f.emit(Ins{
				Name: "MOVE_R",
				Args: []Value{ Reg(reg), Reg(seq[0]) },
			})
		}
	}

	return n
}

// Generate psuedo-instructions for the epilog to a call to a procedure. Returns
// the number of psuedo-instructions generated.
func (f *frontend) procCallEpilog(args []Value) int {
	// See depSeqs definition for info about dependency sequences.
	regSeqs, numSeqs := f.depSeqs(args)

	var n int

	// Generate psuedo-instructions for handling dep seqs that start with
	// numbers.
	for _, seq := range numSeqs {
		for i := 1; i < len(seq); i++ {
			n += f.emit(Ins{
				Name: "MOVE_R",
				Args: []Value{ Reg(seq[i]), Reg(seq[i-1]) },
			})
		}
		n += f.emit(Ins{
			Name: "POP_R",
			Args: []Value{ Reg(seq[len(seq)-1]) },
		})
	}

	// Generate psuedo-instructions for handling dep seqs that start with
	// registers.
	for reg, seq := range regSeqs {
		// Handle cyclic dep seqs.
		if i := len(seq)-1; reg == seq[i] {
			n += f.emit(Ins{
				Name: "PUSH_R",
				Args: []Value{ Reg(seq[0]) },
			})
		} else {
			n += f.emit(Ins{
				Name: "MOVE_R",
				Args: []Value{ Reg(seq[0]), Reg(reg) },
			})
		}

		for i := 1; i < len(seq); i++ {
			n += f.emit(Ins{
				Name: "MOVE_R",
				Args: []Value{ Reg(seq[i]), Reg(seq[i-1]) },
			})
		}
		n += f.emit(Ins{
			Name: "POP_R",
			Args: []Value{ Reg(seq[len(seq)-1]) },
		})
	}

	return n
}

// Returns "dependency sequences" for generating instructions to perform
// maximally in-place, stack-assisted register reorderings that occurs in call
// prologs/epilogs. A dependency sequence A, B, C means:
//
//   (a) for call prologs, A->B must happen after B->C must happen after C is
//       pushed on the stack, and
//
//   (b) for call epilogs, A<-B must happen before B<-C must happen before the
//       stack is popped into C.
//
// A cyclic dependency sequence like A, B, A is valid and results in a circular
// shift of contents of registers in the sequence. The length of a dependency
// sequence is always at least 2.
func (f *frontend) depSeqs(args []Value) (regSeqs map[int][]int, numSeqs map[int][]int) {
	if len(args) == 0 {
		return make(map[int][]int), make(map[int][]int)
	}

	// These helper data structures encode the register transfers that must
	// result from the call prologs/epilogs that are to be generated. For
	// example, if reg R is passed as argument A, then:
	//
	//   (a) after the prolog executes, the contents of reg R must have been
	//       placed into reg A, and
	//
	//   (b) after the epilog executes, the contents of reg A must have been
	//       placed back into reg R.
	//
	// The requirements of this example would be represented in the helper data
	// structures by the fact that reg[R]=A and free[R]=true.
	var (
		// The key-value pair (A, B) means we need to move num A into reg B.
		nums = make(map[int]int)

		// The combination of regs[A]=B and free[A]=true means we need to move
		// reg A's contents into reg B.
		regs = make([]int, f.conf.RegCount)
		free = make([]bool, f.conf.RegCount)
	)
	for dst, src := range args {
		switch src := src.(type) {
		case Reg:
			regs[int(src)] = dst
			free[int(src)] = true
		case Num:
			nums[int(src)] = dst
		}
	}

	// The key-value pair (A, B) represents the dependency sequence A, B[0],
	// B[1], ..., B[N] where N == len(B)-1. The keys of numSeqs represent
	// numbers whereas the keys of regSeqs represent registers.
	numSeqs = make(map[int][]int)
	regSeqs = make(map[int][]int)

	// Find dependency sequences starting with numbers. Order does not matter
	// (hence nums is a map) because numbers are trivially guaranteed to start
	// dependency sequences. Put another way, we don't have to:
	//
	//   (a) in the prolog, worry about something needing to be "moved into a
	//       number" (as opposed to "moved into a register"), or
	//
	//   (b) in the epilog, worry about "restoring the contents of a number" (as
	//       opposed to "restoring the contents of a register")
	//
	// because neither of those things even make sense (hence trivial).
	for num, dst := range nums {
		numSeqs[num] = []int{dst}
		for free[dst] {
			dst, free[dst] = regs[dst], false
			numSeqs[num] = append(numSeqs[num], dst)
		}
	}

	// Find dependency sequences starting with registers.
	var reg, dst int
	for {
		// It is important that we look for dependency sequences starting with the
		// highest register as it guarantees we never start in the middle of a
		// dependency sequence. Formal proof of this in the works. Numbers are
		// trivially guaranteed to start dependency sequences because they are
		// just values and cannot be written into.
		for reg = f.conf.RegCount-1; !free[reg]; reg-- {
			if reg <= 0 { return }
		}
		dst, free[reg] = regs[reg], false

		regSeqs[reg] = []int{dst}
		for free[dst] {
			dst, free[dst] = regs[dst], false
			regSeqs[reg] = append(regSeqs[reg], dst)
		}
	}
}
