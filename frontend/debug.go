package frontend

import (
	"fmt"
)

import (
	"gitlab.com/ileinbach/imp/utils"
)

type Debug interface{
	Debug(int, bool, string, ...interface{})
}

const (
	lexPrefix     string = "[  LEX  ] "
	parsePrefix   string = "[ PARSE ] "
	flattenPrefix string = "[FLATTEN] "
)

func (l *lexer) Debug(verbosity int, prefix bool, format string, a ...interface{}) {
	if l.conf.LexVerbosity >= verbosity {
		out := fmt.Sprintf(format, a...)
		if prefix {
			out = utils.PrefixLines(out, lexPrefix)
		}
		fmt.Print(out)
	}
}

func (p *parser) Debug(verbosity int, prefix bool, format string, a ...interface{}) {
	if p.conf.ParseVerbosity >= verbosity {
		out := fmt.Sprintf(format, a...)
		if prefix {
			out = utils.PrefixLines(out, parsePrefix)
		}
		fmt.Print(out)
	}
}

func (f *frontend) Debug(verbosity int, prefix bool, format string, a ...interface{}) {
	if f.conf.FlattenVerbosity >= verbosity {
		out := fmt.Sprintf(format, a...)
		if prefix {
			out = utils.PrefixLines(out, flattenPrefix)
		}
		fmt.Print(out)
	}
}
